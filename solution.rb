Hole_ = Struct.new(:s,:e,:size) 
Process_ = Struct.new(:s,:e,:size,:id)

j = 4 # number of processes
k = 13 #number of holes
l =  30 
n = 65 #number of times program will run

file = File.open("input.txt")
file1 = File.open("output.txt","w")

total_hole_size = 0
total_process_size = 0
not_allocated_process_bf = 0
not_allocated_process_ff = 0

for q in 1..n
	case_s = file.gets

	holes_1 = []
	holes_2 = []

	x = 0

	label = file.gets
	file1.puts("Case #{q}:")
	for i in 0...k
		r = file.gets.chomp.to_i 
		holes_1 << Hole_.new(x+1,x+r,r)
		holes_2 << Hole_.new(x+1,x+r,r)
		x += r
	end

	memory_size = (holes_1[-1].e)
	file1.puts("Total Memory #{memory_size}")
	total_hole_size += memory_size
	
	process_1 = [] 
	process_2 = [] 
	
	sum_processes = 0

	label = file.gets 

	for i in 0...j 
		size = file.gets.chomp.to_i 
		process_1 << Process_.new(0,0,size,(i+1))
		process_2 << Process_.new(0,0,size,(i+1))
		sum_processes += size
	end

	file1.puts("Sum of all processes #{sum_processes}")
	total_process_size += sum_processes
	
	file1.puts("According to FirstFit :")
	successful_ff = 0
	for process in process_1
		for hole in holes_1 
			if(process.size <= hole.size) 
				file1.print("process no: #{process.id} of size #{process.size}, uses #{hole.s} to #{hole.e}.")
				process.s = hole.s 
				process.e = (process.s + process.size) - 1 
				hole.s = process.e + 1  
				hole.size = hole.e - process.e
				file1.print("Unused memory #{hole.size}\n")
				successful_ff += 1
				break
			end
		end
	end
	not_allocated_process_ff += (j-successful_ff)

	file1.puts("According to BestFit :")
	successful_bf = 0 
	for process in process_2

		holes_2.sort_by!{|x| x.size}

		for hole in holes_2

			if(process.size <= hole.size) 
				file1.print("process no: #{process.id} of size #{process.size} ,uses #{hole.s} to #{hole.e}.")
				process.s = hole.s 
				process.e = (process.s + process.size) - 1 
				hole.s = process.e + 1  
				hole.size = hole.e - process.e
				file1.print("Unused memory #{hole.size}\n")
				successful_bf += 1
				
				break
			end
		end
	end

	not_allocated_process_bf += (j-successful_bf)


	file1.puts("#{successful_ff} processes out of #{j} were allocated by FirstFit")
	file1.puts("#{successful_bf} processes out of #{j} were allocated by BestFit")
	file1.puts("")

end



file1.puts("average memory hole size #{(total_hole_size*1.0)/(n*k)}")
file1.puts("average process size #{(total_process_size*1.0)/(n*j)}")

file1.puts("average number of non allocated processes by FirstFit #{(not_allocated_process_ff*1.0)/n}")
file1.puts("average number of non allocated processes by BestFit #{(not_allocated_process_bf*1.0)/n}")


	